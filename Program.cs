using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metodos
{
    class Program
    {
        static void MyMethod()
        {
            Console.WriteLine("Esto se va a Ejecutar");
        }

        static void MyMethod1(string fname)
        {
            Console.WriteLine(fname + " del CELA");
        }

        static void MyMethod2(string pais = "RD")
        {
            Console.WriteLine(pais);
        }

        static void VariosPara(string fname, int age)
        {
            Console.WriteLine(fname + "es " + age);
        }

        static int MyMethodsuma(int x, int y)
        {
            return x + y;
        }

        static void MyMethodNi(string child1 = "Layanne", string child2 = "Abel", string child3 = "José")
        {
            Console.WriteLine("The youngest child is: " + child3);
        }

        static int PlusMethod(int x, int y)
        {
            return x + y;
        }

        static void Main(string[] args)
        {

            MyMethod();
            MyMethod();
            MyMethod1("Tatiana");
            MyMethod1("Mya");
            MyMethod1("Lucas");
            MyMethod2("Suecia");
            MyMethod2("Costa Rica");
            MyMethod2();
            VariosPara("La edad de Nashla ", 16);
            VariosPara("La edad de Deury ", 17);
            Console.WriteLine(MyMethodsuma(5, 10));
            Console.WriteLine(MyMethodsuma(29, 10));
            Console.WriteLine(MyMethodsuma(20, 6));
            MyMethodNi();

            int myNum1 = PlusMethod(8, 5);
            int myNum2 = PlusMethod(43, 26);
            Console.WriteLine("Int: " + myNum1);
            Console.WriteLine("Double: " + myNum2);


            Console.Read();
        }
    }

}
